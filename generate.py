#!/usr/bin/python
import time
import datetime
import random
import sys
import psycopg2

monthes=["jan","feb","mar","apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]
try:
	conn = psycopg2.connect(host="localhost",database="salary_and_trips", user="postgres", password="postgres")
except(Exception, psycopg2.DatabaseError) as error:
        print(error)
	exit()
cur = conn.cursor()

try:
	cur.execute("""DELETE FROM salary_or_num_trip""")
	cur.execute("""DELETE FROM passport""")
except:
	print "Cannot delete. Exiting..."
	exit()

if len(sys.argv) != 2:
	print("Usage: " + sys.argv[0] + " <numOfLines>")
	
for i in xrange(0,int(sys.argv[1])):
	pass_id = i
	age = random.randint(18,100)
	salary = random.randint(10000, 200000)
	for month in monthes:
		query = "INSERT INTO salary_or_num_trip (pass_id, month, salary_or_num_trip_value) VALUES(%s,%s,%s)"
		data = (pass_id, month, salary)
		cur.execute(query,data)
		trip_num = random.randint(0, 10)
		query = "INSERT INTO salary_or_num_trip (pass_id, month, salary_or_num_trip_value) VALUES(%s,%s,%s)"
		data = (pass_id, month, trip_num)
		cur.execute(query,data)
	query = "INSERT INTO passport (pass_id, age) VALUES(%s,%s)"
	data = (pass_id, age)
	cur.execute(query,data)
conn.commit()
cur.close()
conn.close()