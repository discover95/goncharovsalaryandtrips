import java.util.concurrent.atomic.AtomicLong;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * This class represents syslog record object.
 * @author Dmitry Goncharov
 *
 */
public class MidRecord {
    private static final AtomicLong ID_MID_GEN = new AtomicLong();

    /** ID (indexed). */
    @QuerySqlField(index = true)
    private Long id;

    /** passport number. */
    @QuerySqlField
    private int pass_id;

    /** age. */
    @QuerySqlField
    private int age;

    public MidRecord() {
        // No-op.
    }

    /**
     * Constructor for LogRecord.
     *
     * @param pass_id passport number.
     * @param age
     */
    public MidRecord(int pass_id, int age) {
        id = ID_MID_GEN.incrementAndGet();
        this.pass_id = pass_id;
        this.age = age;
    }

    /**
     *
     * @return passport_id.
     */
    public int pass_id() {
        return pass_id;
    }

    /**
     *
     * @return age.
     */
    public int age() {
        return age;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "InpRecord [id=" + id + ", pass_id=" + pass_id
                + ", age=" + age + ']';
    }
}
