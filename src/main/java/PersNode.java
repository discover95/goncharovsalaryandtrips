import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteDataStreamer;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;

/**
 * Class for import data to persistence.
 */
public class PersNode {
    /**
     * @param args
     */
    public static void main(String[] args) {
        try (Ignite ignite = Ignition.start("config.xml")) {
            CacheConfiguration<Long, InpRecord> cacheSalTrip = new CacheConfiguration<>("cacheSalTrip");
            ignite.active(true);
            cacheSalTrip.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
            cacheSalTrip.setBackups(1);
            cacheSalTrip.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC);
            cacheSalTrip.setIndexedTypes(Long.class, InpRecord.class);
            IgniteCache<Long, InpRecord> inp_cache = ignite.getOrCreateCache(cacheSalTrip);
            inp_cache.clear();
            try (IgniteDataStreamer<Long, InpRecord> stmr = ignite.dataStreamer("cacheSalTrip")) {
                // Stream entries
                File sal_trip_file = new File("input/sal_trip");
                System.out.println("Reading the sal_trip file...");
                try (BufferedReader br = new BufferedReader(new FileReader(sal_trip_file))) {
                    stmr.allowOverwrite(true);
                    long l = 0L;
                    String[] lines;
                    String pass_id;
                    String month;
                    String value;
                    String line;
                    while ((line = br.readLine()) != null) {
//                        System.out.println(line);
                        lines = line.split(",");
                        pass_id = lines[0];
                        month = lines[1];
                        value = lines[2];
                        stmr.addData(l, new InpRecord(Integer.parseInt(pass_id), month,
                                Integer.parseInt(value)));
                        l++;
                    }
                    System.out.println("File sal_trip is read... ");
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            CacheConfiguration<Long, MidRecord> cachePassport = new CacheConfiguration<>("cachePassport");
//            ignite.active(true);
            cachePassport.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
            cachePassport.setBackups(1);
            cachePassport.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC);
            cachePassport.setIndexedTypes(Long.class, MidRecord.class);
            IgniteCache<Long, MidRecord> mid_cache = ignite.getOrCreateCache(cachePassport);
            mid_cache.clear();
            try (IgniteDataStreamer<Long, MidRecord> stmr1 = ignite.dataStreamer("cachePassport")) {
                // Stream entries.
                File pass_file = new File("input/pass");
                System.out.println("Reading the pass file...");
                try (BufferedReader br = new BufferedReader(new FileReader(pass_file))) {
                    stmr1.allowOverwrite(true);
                    long l = 0L;
                    String[] lines;
                    String pass_id;
                    String age;
                    String line;
                    while ((line = br.readLine()) != null) {
//                        System.out.println(line);
                        lines = line.split(",");
                        pass_id = lines[0];
                        age = lines[1];
                        stmr1.addData(l, new MidRecord(Integer.parseInt(pass_id),
                                Integer.parseInt(age)));
                        l++;
                    }
                    System.out.println("File pass is read... ");
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
}
