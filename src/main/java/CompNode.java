import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.compute.ComputeJob;
import org.apache.ignite.compute.ComputeJobAdapter;
import org.apache.ignite.compute.ComputeJobResult;
import org.apache.ignite.compute.ComputeTaskSplitAdapter;

public class CompNode {
    /**
     * Start up an empty node with compute configuration.
     *
     * @param args Command line arguments, none required.
     * @throws IgniteException If failed.
     */
    public static void main(String[] args) throws IgniteException {
        try (Ignite ignite = Ignition.start("config.xml")) {
            ignite.active(true);

            IgniteCache<Long, MidRecord> cache1 = ignite.cache("cachePassport");
            IgniteCache<Long, InpRecord> cache = ignite.cache("cacheSalTrip");
            IgniteCompute compute = ignite.compute();
            Map<Integer, Integer> passports = new TreeMap<Integer, Integer>();
            SqlFieldsQuery sql_pass = new SqlFieldsQuery("select pass_id, age from MidRecord");
            try (QueryCursor<List<?>> cursor = cache1.query(sql_pass)) {
                for (List<?> row : cursor) {
//                    System.out.println(row.get(0)+"-"+row.get(1));
                    passports.put((Integer) row.get(0), (Integer) row.get(1));
                }
            }
//            System.out.println("1___cache___");

            SqlFieldsQuery sql = new SqlFieldsQuery("select pass_id, month, value from InpRecord");
            try (QueryCursor<List<?>> cursor = cache.query(sql)) {
                List<InpRecord> records = new ArrayList<InpRecord>();
                for (List<?> row : cursor) {
//                    System.out.println("2___cache___"+row.get(0));
                    if (passports.containsKey((Integer) row.get(0))) {
//                        System.out.println(row.get(0)+"-"+passports.get((Integer) row.get(0))+" "+(String) row.get(1)+
//                                " "+(Integer) row.get(2));
                        InpRecord log = new InpRecord(passports.get((Integer) row.get(0)), (String) row.get(1),
                                (Integer) row.get(2));
                        records.add(log);
                    }
                }
//                System.out.println("3___cache___"+records.size());
                // Execute task on the cluster and wait for its completion.
                Map<String, OutRecord> map = compute.execute(MergePassTask.class, records);
//                System.out.println("4___cache___");

//                for (OutRecord cr : map.values()) {
//                    System.out.println(cr.getKey() + " ");
//                    for (String key : cr.getMap().keySet()){
//                        System.out.println("  " + key + " " +
//                                cr.getMap().get(key).getSalary()/cr.getMap().get(key).getNumber() +
//                                ": " + cr.getMap().get(key).getNumTrip()/cr.getMap().get(key).getNumber() +
//                                " number:" + cr.getMap().get(key).getNumber());
//                    }
//                }

                List<OutRecord> avg_records = new ArrayList<OutRecord>();
                for (OutRecord otp : map.values()){
                    avg_records.add(otp);
                }
                Map<String, OutRecord> av_map = compute.execute(AveragePassTask.class, avg_records);
                for (OutRecord avg_cr : av_map.values()) {
                    System.out.println(avg_cr.getKey());
                    for (String key : avg_cr.getMap().keySet()){
                        System.out.println("  " + key + " " +
                                avg_cr.getMap().get(key).getSalary() +
                                ": " + avg_cr.getMap().get(key).getNumTrip());
                    }
                }
            }
        }
    }

    /**
     * Task to sum salary and trip count.
     */
    private static class MergePassTask
            extends ComputeTaskSplitAdapter<List<InpRecord>, Map<String, OutRecord>> {

        private static final long serialVersionUID = 1L;
        // 1. Splits the received list of records into to LogRecord instances
        // 2. Creates a child job for each record
        // 3. Sends created jobs to other nodes for processing.
        @Override
        protected Collection<? extends ComputeJob> split(int clusterSize, List<InpRecord> arg) {
//            System.out.println("1___split___");
            List<ComputeJob> jobs = new ArrayList<>(arg.size());
            for (final InpRecord log : arg) {
                jobs.add(new ComputeJobAdapter() {
                    private static final long serialVersionUID = 1L;
                    @Override
                    public Object execute() {
                        // Return LogRecord.
                        return new OutRecord(log);
                    }
                });
            }
//            System.out.println("2___split___");
            return jobs;
        }

        @Override
        public Map<String, OutRecord> reduce(List<ComputeJobResult> results) {
//            System.out.println("1___reduce___");
            Map<String, OutRecord> map = new TreeMap<String, OutRecord>();
            for (ComputeJobResult res : results) {
                OutRecord cr = res.<OutRecord>getData();
                String key = cr.getKey();
//                System.out.println(key + " ");
                if (map.containsKey(key)) {
                    cr.merge(map.get(key));
                    map.put(key, cr);
                } else {
                    map.put(key, cr);
                }
            }
//            System.out.println("2___reduce___");
            return map;
        }
    }

    /**
     * Task to aver sum salary and trip count.
     */
    private static class AveragePassTask
            extends ComputeTaskSplitAdapter<List<OutRecord>, Map<String, OutRecord>> {

        private static final long serialVersionUID = 1L;

        // 1. Splits the received list of records into to LogRecord instances
        // 2. Creates a child job for each record
        // 3. Sends created jobs to other nodes for processing.
        @Override
        protected Collection<? extends ComputeJob> split(int clusterSize, List<OutRecord> arg) {
            List<ComputeJob> jobs = new ArrayList<>(arg.size());
            for (final OutRecord log : arg) {
                jobs.add(new ComputeJobAdapter() {
                    private static final long serialVersionUID = 1L;
                    @Override
                    public Object execute() {
                        return log;
                    }
                });
            }
            return jobs;
        }

        @Override
        public Map<String, OutRecord> reduce(List<ComputeJobResult> results) {
            Map<String, OutRecord> map = new TreeMap<String, OutRecord>();
            for (ComputeJobResult res : results) {
                OutRecord cr = res.<OutRecord>getData();
                String key = cr.getKey();
                for (String month : cr.getMap().keySet()) {
                    cr.getMap().get(month).setSalary(cr.getMap().get(month).getSalary() * 2 / cr.getMap().get(month).getNumber());
                    cr.getMap().get(month).setNumTrip(cr.getMap().get(month).getNumTrip() * 2 / cr.getMap().get(month).getNumber());
                }
                map.put(key, cr);
            }
            return map;
        }
    }
}
