import java.util.concurrent.atomic.AtomicLong;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * This class represents syslog record object.
 * @author Dmitry Goncharov
 *
 */
public class InpRecord {
    private static final AtomicLong ID_GEN = new AtomicLong();

    /** ID (indexed). */
    @QuerySqlField(index = true)
    private Long id;

    /** passport number. */
    @QuerySqlField
    private int pass_id;

    /** month. */
    @QuerySqlField
    private String month;

    /** if (month == "") then age else salary or number of trip. */
    @QuerySqlField
    private int value;

    public InpRecord() {
        // No-op.
    }

    /**
     * Constructor for LogRecord.
     *
     * @param pass_id passport number.
     * @param month month.
     * @param value if (month == "") then age else salary or number of trip.
     */
    public InpRecord(int pass_id, String month, int value) {
        id = ID_GEN.incrementAndGet();
        this.pass_id = pass_id;
        this.month = month;
        this.value = value;
    }

    /**
     *
     * @return passport_id.
     */
    public int pass_id() {
        return pass_id;
    }

    /**
     *
     * @return month.
     */
    public String month() {
        return month;
    }

    /**
     *
     * @return value.
     */
    public int value() {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        if (value <= 100) {
            return "InpRecord [id=" + id + ", pass_id=" + pass_id
                    + ", month=" + month + ", num_trip=" + value + ']';
        }
        else{
            return "InpRecord [id=" + id + ", pass_id=" + pass_id
                    + ", month=" + month + ", salary=" + value + ']';
        }
    }
}
