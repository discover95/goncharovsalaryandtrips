import java.util.Map;
import java.util.TreeMap;

/**
 * Class for storage information about message's priority level and their counts for hour.
 *
 * @author Dmitry Goncharov.
 */
public class OutRecord {
    private String age_group; //first member of pair
    private Map<String, PairSalTrip> map;

    /**
     * @param age_group
     * @param month
     * @param salary
     * @param num_trip
     */
    public OutRecord(String age_group, String month, int salary, int num_trip) {
        this.age_group = age_group;
        map = new TreeMap<String, PairSalTrip>();
        PairSalTrip pair_sal_trip = new PairSalTrip(salary, num_trip, 0);
        map.put(month, pair_sal_trip);
    }

    /**
     * @param log InpRecord instance.
     */
    public OutRecord(InpRecord log) {
        this.age_group = getAgeCategory(log.pass_id());
        map = new TreeMap<String, PairSalTrip>();
        PairSalTrip pair_sal_trip = new PairSalTrip(0, 0, 1);
        if (log.value() <= 100) {
            pair_sal_trip.setNumTrip(log.value());
        }
        else{
            pair_sal_trip.setSalary(log.value());
        }
        map.put(log.month(), pair_sal_trip);
    }

    /**
     * @return Aage_group.
     */
    public String getKey() {
        return age_group;
    }

    public Map<String, PairSalTrip> getMap(){
        return map;
    }

    /**
     * Method for merging two CountedRecord instances.
     *
     * @param record a CountedRecord instances to merge.
     */
    public void merge(OutRecord record) {
        if (age_group == record.getKey()) {
            for(String key : record.getMap().keySet()){
                PairSalTrip pair_sal_trip = record.getMap().get(key);
                if(map.containsKey(key)){
                    map.get(key).incSalary(pair_sal_trip.getSalary());
                    map.get(key).incNumTrip(pair_sal_trip.getNumTrip());
                    map.get(key).incNumber(pair_sal_trip.getNumber());
                    PairSalTrip pair_sal_trip1 = new PairSalTrip(map.get(key).getSalary(),
                            map.get(key).getNumTrip(), map.get(key).getNumber());
                    map.put(key, pair_sal_trip1);
                }
                else{
                    map.put(key, pair_sal_trip);
                }
//                System.out.println("  " + key + " " + map.get(key).getSalary()
//                        + " " + map.get(key).getNumTrip());
            }
        }
    }

    @Override
    public String toString() {
        return "OutRecord [age_group" + age_group + ", map=" + map +"]";
    }

    /**
     * @param age number.
     * @return a String, age_category.
     */
    public static String getAgeCategory(int age) {
        String age_category = null;
        if (age <= 25){
            age_category = "-25";
        }
        else if (age > 25 && age <= 35){
            age_category = "26-35";
        }
        else if (age > 35 && age <= 45){
            age_category = "36-45";
        }
        else if (age > 45 && age <= 60){
            age_category = "46-60";
        }
        else{
            age_category = "61-";
        }
        return age_category;
    }

    public static class PairSalTrip{
        private Integer salary;
        private Integer num_trip;
        private Integer number;

        public PairSalTrip (Integer salary, Integer num_trip, Integer number){
                this.salary = salary;
                this.num_trip = num_trip;
                this.number = number;
        }

        public void setSalary (Integer salary){
            this.salary = salary;
        }

        public void incSalary (Integer salary){ this.salary += salary; }

        public void setNumTrip (Integer num_trip){
            this.num_trip = num_trip;
        }

        public void incNumTrip (Integer num_trip){
            this.num_trip += num_trip;
        }

        public void setNumber (Integer number){
            this.number = number;
        }

        public void incNumber (Integer number){
            this.number += number;
        }

        public int getSalary(){
            return this.salary;
        }

        public int getNumTrip(){
            return this.num_trip;
        }

        public int getNumber(){
            return this.number;
        }

        @Override
        public boolean equals(Object o) {
//            if (this.equals(o))
                return true;
        }

        @Override
        public int hashCode() {
            return this.salary.hashCode();
        }
    }

}