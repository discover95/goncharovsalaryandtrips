import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * @author Dmitry Goncharov
 *
 */
public class InpRecordTest {
    /**
     * Test method for {@link InpRecord#pass_id()}.
     */
    @Test
    public final void testPriority() {
        InpRecord inp = new InpRecord(1, "dec", 5);
        assertTrue(inp.pass_id() == 1);
    }

    /**
     * Test method for {@link InpRecord#month()}.
     */
    @Test
    public final void testMonth() {
        InpRecord inp = new InpRecord(1, "dec", 5);
        assertTrue(inp.month() == "dec");
    }

    /**
     * Test method for {@link InpRecord#value()}.
     */
    @Test
    public final void testValue() {
        InpRecord inp = new InpRecord(1, "dec", 5);
        assertTrue(inp.value() == 5);
    }
}
