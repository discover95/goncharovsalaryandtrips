import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * @author Dmitry Goncharov
 *
 */
public class MidRecordTest {
    /**
     * Test method for {@link MidRecord#pass_id()}.
     */
    @Test
    public final void testPriority() {
        MidRecord mid = new MidRecord(1, 25);
        assertTrue(mid.pass_id() == 1);
    }

    /**
     * Test method for {@link MidRecord#age()}.
     */
    @Test
    public final void testValue() {
        MidRecord mid = new MidRecord(1, 25);
        assertTrue(mid.age() == 25);
    }
}
