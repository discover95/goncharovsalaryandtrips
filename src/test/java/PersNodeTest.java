import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Dmitry Goncharov.
 *
 */
public class PersNodeTest {

    static Ignite ignite;
    static IgniteCache<Long, InpRecord> inp_cache;
    static IgniteCache<Long, MidRecord> mid_cache;
    static File sal_trip_file;
    static File pass_file;

    /**
     *
     *
     */
    @BeforeClass
    public static void setUp() throws Exception {
        OutputStream os = new ByteArrayOutputStream();
        System.setOut(new PrintStream(os));
        PersNode.main(null);
        ignite = Ignition.start("config.xml");
        ignite.active(true);
        inp_cache = ignite.cache("cacheSalTrip");
        mid_cache = ignite.cache("cachePassport");
        sal_trip_file = new File("input/sal_trip");
        pass_file = new File("input/pass");
    }

    /**
     *
     *
     */
    @AfterClass
    public static void tearDown() throws Exception {
        ignite.close();
        System.setOut(System.out);
    }

    /**
     * Test method for {@link PersNode#main(java.lang.String[])}.
     * Check that number of lines in file equals to records number at cache.
     */
    @Test
    public final void testMainCount() {
        int rowCount = 0;
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(sal_trip_file))) {
            while ((line = br.readLine()) != null) {
                rowCount++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlFieldsQuery sql = new SqlFieldsQuery("select count(id) from InpRecord");
        QueryCursor<List<?>> cursor = inp_cache.query(sql);
        Long count = (Long)cursor.getAll().get(0).get(0);
        assertTrue(count == rowCount);
    }

    /**
     * Test method for {@link PersNode#main(java.lang.String[])}.
     * Check that the fourth line of syslog is correctly represented at Ignite Persistence.
     */
    @Test
    public final void testMainRecord() {
        int rowCount = 0;
        InpRecord fourthRecord = null;
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(sal_trip_file))) {
            while ((line = br.readLine()) != null) {
                rowCount++;
                if (rowCount == 4) {
                    String [] lines = line.split(",");
                    fourthRecord = new InpRecord(Integer.parseInt(lines[0]), lines[1],
                            Integer.parseInt(lines[2]));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String text_query = "select pass_id, month, value from InpRecord where " +
                "pass_id="+fourthRecord.pass_id()+
                " and month='"+fourthRecord.month()+"'"+
                " and value="+fourthRecord.value();
        SqlFieldsQuery sql = new SqlFieldsQuery(text_query);
        QueryCursor<List<?>> cursor = inp_cache.query(sql);
        List<?> row = cursor.getAll().get(0);
        assertTrue(fourthRecord.pass_id() == (Integer)row.get(0));
        assertTrue(fourthRecord.month().equals(row.get(1)));
        assertTrue(fourthRecord.value() == (Integer)row.get(2));
    }
}
