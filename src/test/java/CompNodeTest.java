import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Dmitry Goncharov.
 *
 */
public class CompNodeTest {

    Ignite ignite;
    IgniteCache<Long, InpRecord> cache1;
    IgniteCache<Long, MidRecord> cache2;
    File file1;
    File file2;
    OutputStream os;
    PrintStream ps;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        PersNode.main(null);
    }

    /**
     * @throws java.lang.Exception
     *
     */
    @Before
    public void setUp() throws Exception {
        file1 = new File("input/pass");
        file2 = new File("input/sal_trip");

        os = new ByteArrayOutputStream();
        ps = new PrintStream(os);
        System.setOut(ps);
    }

    /**
     * @throws java.lang.Exception
     *
     */
    @After
    public void tearDown() throws Exception {
        System.setOut(System.out);
    }

    /**
     * Test method for {@link CompNode#main(java.lang.String[])}.
     */
    @Test
    public final void testMain() {
        try (PrintWriter writer = new PrintWriter(file1)) {
            writer.println("0,60");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try (PrintWriter writer1 = new PrintWriter(file2)) {
            writer1.println("0,jan,83088");
            writer1.println("0,jan,10");
            writer1.println("0,feb,83088");
            writer1.println("0,feb,8");
            writer1.println("0,mar,83088");
            writer1.println("0,mar,3");
            writer1.println("0,apr,83088");
            writer1.println("0,apr,4");
            writer1.println("0,may,83088");
            writer1.println("0,may,8");
            writer1.println("0,jun,83088");
            writer1.println("0,jun,2");
            writer1.println("0,jul,83088");
            writer1.println("0,jul,1");
            writer1.println("0,aug,83088");
            writer1.println("0,aug,5");
            writer1.println("0,sep,83088");
            writer1.println("0,sep,10");
            writer1.println("0,oct,83088");
            writer1.println("0,oct,6");
            writer1.println("0,nov,83088");
            writer1.println("0,nov,0");
            writer1.println("0,dec,83088");
            writer1.println("0,dec,0");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        PersNode.main(null);
        CompNode.main(null);
        StringBuilder sb = new StringBuilder();
        sb.append("46-60\n");
        sb.append("  apr 83088: 4\n");
        sb.append("  aug 83088: 5\n");
        sb.append("  dec 83088: 0\n");
        sb.append("  feb 83088: 8\n");
        sb.append("  jan 83088: 10\n");
        sb.append("  jul 83088: 1\n");
        sb.append("  jun 83088: 2\n");
        sb.append("  mar 83088: 3\n");
        sb.append("  may 83088: 8\n");
        sb.append("  nov 83088: 0\n");
        sb.append("  oct 83088: 6\n");
        sb.append("  sep 83088: 10\n");
        assertTrue(os.toString().contains(sb.toString()));

    }
}
