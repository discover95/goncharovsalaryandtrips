import static org.junit.Assert.assertTrue;
import java.util.Map;
import java.util.TreeMap;
import org.junit.Test;

/**
 * @author Dmitry Goncharov.
 *
 */
public class OutRecordTest {

    /**
     * Test method for {@link OutRecord#getKey()}}.
     */
    @Test
    public void testGetHour() {
        OutRecord cr = new OutRecord("-25", "dec", 50000, 3);
        assertTrue(cr.getKey() == "-25");
    }

    /**
     * Test method for {@link OutRecord#getMap()}.
     */
    @Test
    public void testGetMap() {
        OutRecord cr = new OutRecord("-25", "dec", 50000, 3);
        OutRecord.PairSalTrip pair_sal_trip = new OutRecord.PairSalTrip(50000, 3, 1);
        Map<String, OutRecord.PairSalTrip> map = new TreeMap<String, OutRecord.PairSalTrip>();
        map.put("dec", pair_sal_trip);
        assertTrue(cr.getMap().equals(map));
    }

    /**
     * Test method for {@link OutRecord#merge(OutRecord)}.
     */
    @Test
    public void testMerge() {
        OutRecord cr1 = new OutRecord("-25", "dec", 10000, 1);
        OutRecord cr2 = new OutRecord("-25", "sep", 50000, 5);
        OutRecord cr3 = new OutRecord("26-35", "dec", 10000, 1);
        OutRecord cr4 = new OutRecord("26-35", "dec", 20000, 2);
        cr1.merge(cr2);
        cr3.merge(cr4);
        Map<String, OutRecord.PairSalTrip> map = new TreeMap<String, OutRecord.PairSalTrip>();
        OutRecord.PairSalTrip pair_sal_trip1 = new OutRecord.PairSalTrip(10000, 1, 1);
        OutRecord.PairSalTrip pair_sal_trip2 = new OutRecord.PairSalTrip(50000, 5, 1);
        map.put("dec", pair_sal_trip1);
        map.put("sep", pair_sal_trip2);
        Map<String, OutRecord.PairSalTrip> map2 = new TreeMap<String, OutRecord.PairSalTrip>();
        OutRecord.PairSalTrip pair_sal_trip3 = new OutRecord.PairSalTrip(30000, 3, 1);
        map2.put("dec", pair_sal_trip3);
        assertTrue(cr1.getKey() == "-25");
        assertTrue(cr3.getKey() == "26-35");
        assertTrue(cr1.getMap().equals(map));
        assertTrue(cr3.getMap().equals(map2));
    }

}
